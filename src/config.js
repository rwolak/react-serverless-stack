export default {
    MAX_ATTACHMENT_SIZE: 5000000,
    s3: {
        REGION: "eu-central-1",
        BUCKET: "react-tutorial-notes-app-uploads"
    },
    apiGateway: {
        REGION: "eu-central-1",
        URL: "https://8i11ut789h.execute-api.eu-central-1.amazonaws.com/prod"
    },
    cognito: {
        REGION: "eu-central-1",
        USER_POOL_ID: "eu-central-1_bPsKjeeQa",
        APP_CLIENT_ID: "5flv1044quqrdqobkr0cho9o66",
        IDENTITY_POOL_ID: "eu-central-1:982656a6-6472-4df6-b378-8ed165b9ce77"
    }
};